/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const Controller = new (class
{
    update()
    {
        const gamepad = navigator.getGamepads()[0];
        const stickLeft = {x: gamepad.axes[0], y: gamepad.axes[1]};

        if(gamepad.buttons[0].pressed || (gamepad.axes.length >= 6 && gamepad.axes[5] > 0.0)) 
        {
            arduinoStatus.triggerShoot();
        } else {
            arduinoStatus.triggerDefaultPos();
        }

        this.updateStick(stickLeft);
        requestAnimationFrame(() => this.update());
    }

    updateStick(stick) 
    {
        const tankSpeed = this.mapStickToTank(stick);

        arduinoStatus.setMotorSpeed(Motors.LEFT, Math.abs(tankSpeed.left));
        arduinoStatus.setMotorSpeed(Motors.RIGHT, Math.abs(tankSpeed.right));

        arduinoStatus.setMotorDirection(Motors.LEFT, this.speedToDirection(tankSpeed.left));
        arduinoStatus.setMotorDirection(Motors.RIGHT, this.speedToDirection(tankSpeed.right));

        updateInfoUi(stick, tankSpeed);

        arduinoStatus.update();
    }

    speedToDirection(speed)
    {
        return speed == 0.0 ? MotorDirection.STOPPED : (
            speed > 0.0 ? MotorDirection.FORWARDS : MotorDirection.BACKWARDS
        );
    }

    mapStickToTank(stick)
    {
        const stickAbs = {x: Math.abs(stick.x), y: Math.abs(stick.y)};
        let speed = Math.min(
            Math.sqrt(Math.pow(stickAbs.x, 2) + Math.pow(stickAbs.y, 2)), 
        1.0);

        if(speed < 0.2) {
            return {
                left:  0.0,
                right: 0.0
            };    
        }

        if(stick.x < 0.0) {
            speed *= -1.0;
        }

        const angleDeg = Math.atan(stick.y / stick.x) * (180 / Math.PI);

        return {
            left:  (1.0 - (Math.max(angleDeg, 0.0) / 45.0)) * speed, 
            right: ((Math.max(-angleDeg, 0.0) / 45.0) - 1.0) * speed
        };
    }
})();