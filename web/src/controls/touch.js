
/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const DOUBLE_TOUCH_TIME = 250;
const TOUCH_COLOR   = 'rgba(255, 165, 0, 1)';

class TouchControls
{
    constructor()
    {
        this.doubleTouchLastTime = 0;
        this.touchTimeout = undefined;

        const touchNode = document.querySelector('.touchContainer');
        const crossNode = document.getElementById("target-cross");

        const dynamic = nipplejs.create({
            zone: touchNode,
            color: TOUCH_COLOR,
            size: 200,
        });

        crossNode.addEventListener('touchstart', () => {
            arduinoStatus.triggerShoot();
            arduinoStatus.update(); 
        });

        crossNode.addEventListener('touchend', () => {
            arduinoStatus.triggerDefaultPos();
            arduinoStatus.update(); 
        });

        dynamic.on('start', (evt, data) => {
            arduinoStatus.triggerDefaultPos();
            arduinoStatus.update();
        });

        dynamic.on('move', (evt, data) => {

            const force = Math.min(data.force, 1.0);

            const vector = {
                x: Math.cos(data.angle.radian) * force,
                y: Math.sin(data.angle.radian) * -force
            };
            
            Controller.updateStick(vector);
        });

        dynamic.on('end', (evt, data) => {
            Controller.updateStick({x: 0.0, y: 0.0});
        });
    }
};