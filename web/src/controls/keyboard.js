/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const Keyboard = new (class
{
    constructor()
    {
        window.addEventListener("keydown", e => this.onKey(e.code, true));
        window.addEventListener("keyup", e => this.onKey(e.code, false));

        this.speed = 1.0;
    }

    onKey(keyName, isDown)
    {
        let directionL = MotorDirection.STOPPED;
        let directionR = MotorDirection.STOPPED;

        switch(keyName) 
        {
            case "ArrowUp":
            case "KeyW":
                directionL = directionR = isDown ? MotorDirection.FORWARDS : MotorDirection.STOPPED;
            break;

            case "ArrowDown":
            case "KeyS":
                directionL = directionR = isDown ? MotorDirection.BACKWARDS : MotorDirection.STOPPED;
            break;

            case "ArrowLeft":
            case "KeyA":
                if(isDown) 
                {
                    directionL = MotorDirection.BACKWARDS;
                    directionR = MotorDirection.FORWARDS;
                }
            break;

            case "ArrowRight":
            case "KeyD":
                if(isDown)
                {
                    directionL = MotorDirection.FORWARDS;
                    directionR = MotorDirection.BACKWARDS;
                }
            break;

            case "Space":
                if(isDown){
                    arduinoStatus.triggerShoot();
                } else {
                    arduinoStatus.triggerDefaultPos();
                }
            break;
        }

        const tankSpeed = {
            left: directionL != MotorDirection.STOPPED ? this.speed : 0.0,
            right: directionR != MotorDirection.STOPPED ? this.speed : 0.0
        };

        arduinoStatus.setMotorSpeed(Motors.LEFT, tankSpeed.left);
        arduinoStatus.setMotorSpeed(Motors.RIGHT, tankSpeed.right);

        arduinoStatus.setMotorDirection(Motors.LEFT, directionL);
        arduinoStatus.setMotorDirection(Motors.RIGHT, directionR);
        
        arduinoStatus.update();

        updateInfoUi({x:0.0, y:0.0}, {
            left: tankSpeed.left * this.getDirectionMulti(directionL),
            right: tankSpeed.right * this.getDirectionMulti(directionR),
        });
    }

    getDirectionMulti(direction)
    {
        if(direction == MotorDirection.STOPPED)
            return 0.0;

        return direction == MotorDirection.FORWARDS ? 1.0 : -1.0;
    }

})();