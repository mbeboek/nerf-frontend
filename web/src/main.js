/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

let stream = undefined;

function updateBitmask(container, byteValue)
{
    [...container.children].reverse().forEach((div, idx) => 
    {
        if(byteValue & (1 << idx)) {
            div.classList.add("active");
        } else {
            div.classList.remove("active");
        }
    });
}

function updateInfoUi(stickLeft, tankSpeed)
{
    CanvasHelper.drawControlStick(canvasStickLeft, stickLeft);

    CanvasHelper.drawChainBar(canvasChainLeft, tankSpeed.left);
    CanvasHelper.drawChainBar(canvasChainRight, tankSpeed.right);
}

function toggleFullScreen() {
    var doc = window.document;
    var docEl = doc.documentElement;

    var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
    var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

    if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
        requestFullScreen.call(docEl);
    }
    else {
        cancelFullScreen.call(doc);
    }
}

function shoot()
{
    console.log("shoot");
    arduinoStatus.triggerShoot();
    arduinoStatus.update();

    setTimeout(() => {
        arduinoStatus.triggerDefaultPos();   
        arduinoStatus.update();
    }, 500);
}

function startStream()
{
    stream.start();
    document.getElementById("streamButton").hidden = true;
}

function init()
{
    const touchCtrl = new TouchControls();
    const cameraHost = location.hostname;

    stream = new WebRTCVideoStream(document.getElementById("video"), cameraHost);

    const checkboxHtml = `<div></div>`.repeat(8);
    controlByteContainer.innerHTML = checkboxHtml;
    speedLByteContainer.innerHTML = checkboxHtml;
    speedRByteContainer.innerHTML = checkboxHtml;

    updateInfoUi({x: 0.0, y: 0.0}, {left: 0.0, right: 0.0});
    setInterval(() => {
        statsTime.innerHTML = new Date().toISOString();
    }, 1000);

    window.addEventListener("gamepadconnected", e =>
    {
        console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
            e.gamepad.index, e.gamepad.id,
            e.gamepad.buttons.length, e.gamepad.axes.length
        );
        Controller.update();
    })
}