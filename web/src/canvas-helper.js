/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const CANVAS_COLOR_BORDER = 'rgba(200,200,200, 1)';
const CANVAS_COLOR_DATA   = 'rgba(255, 165, 0, 1)';

const CanvasHelper = 
{
    drawControlStick: (canvas, pos) =>
    {
        const {width, height} = canvas;
        const halfSize = {x: width * 0.5, y: height * 0.5};
        const minHalfSize = Math.min(halfSize.x, halfSize.y);

        // bounding circle
        const ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, width, height);
        ctx.strokeStyle = CANVAS_COLOR_BORDER;
        ctx.beginPath();
        ctx.arc(halfSize.x, halfSize.y, minHalfSize - 2, 0, 2*Math.PI);
        ctx.stroke();

        // direction vector
        ctx.strokeStyle = CANVAS_COLOR_DATA;
        ctx.lineWidth=2;
        ctx.beginPath();
        ctx.moveTo(minHalfSize, minHalfSize);
        ctx.lineTo(pos.x * minHalfSize + minHalfSize, pos.y * minHalfSize + minHalfSize);
        ctx.stroke();
        
        ctx.lineWidth=1;
    },

    drawChainBar: (canvas, speed) => 
    {
        const {width, height} = canvas;
        const halfSize = {x: width * 0.5, y: height * 0.5};

        const ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, width, height);

        // border rect
        ctx.strokeStyle = CANVAS_COLOR_BORDER;
        ctx.strokeRect(0, 0, width, height);

        // speed rect
        ctx.fillStyle = CANVAS_COLOR_DATA;
        ctx.fillRect(0, halfSize.y, width, -speed * halfSize.y);

        // middle line
        ctx.strokeStyle = CANVAS_COLOR_BORDER;
        ctx.beginPath();
        ctx.moveTo(0, halfSize.y);
        ctx.lineTo(width, halfSize.y);
        ctx.stroke();
    },
};