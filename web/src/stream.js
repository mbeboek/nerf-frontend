/**
* @copyright 2018 - Max Bebök
* @author Code rewritten and originally taken from the UV4L web-interface
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

RTCPeerConnection = window.RTCPeerConnection || window.webkitRTCPeerConnection;
navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia || navigator.msGetUserMedia;

class WebRTCVideoStream
{
    constructor(videoNode, cameraHost)
    {
        this.videoNode = videoNode;
        this.cameraHost = cameraHost;

        this.webSocketUrl = `ws://${this.cameraHost}:8080/stream/webrtc`;
        this.webSocket;
        this.peerConnection;

        this.mediaConstraints = {
            optional: [],
            mandatory: {
                OfferToReceiveAudio: true,
                OfferToReceiveVideo: true
            }
        };

        this.rtcRequestJson = JSON.stringify({
            what: "call",
            options: {
                force_hw_vcodec: false,
                vformat: 60,
            }
        });

        window.onbeforeunload = () => this.stop();
    }

    createPeerConnection() 
    {
        try {
            this.peerConnection = new RTCPeerConnection();
            this.peerConnection.ontrack = ev => this.setVideo(ev.streams[0]);
            this.peerConnection.onremovestream = () => this.removeVideo();
        } catch (e) {
            console.error("createPeerConnection error: ", e);
        }
    }

    setVideo(stream) {
        this.videoNode.srcObject = stream;
        this.videoNode.play();
    }

    removeVideo() 
    {
        this.videoNode.srcObject = null;
    }

    start() 
    {
        if(!("WebSocket" in window))
        {
            return alert("WebSockets not supported!");
        }

        this.createPeerConnection();

        this.webSocket = new WebSocket(this.webSocketUrl);

        this.webSocket.onopen = () => this.webSocket.send(this.rtcRequestJson);
        this.webSocket.onclose = () => this.closePeerConnection();
        
        this.webSocket.onerror = ev => {
            alert("Error: " + JSON.stringify(ev, null, 4));
            this.webSocket.close();
        };

        this.webSocket.onmessage = ev =>
        {
            const {what, data} = JSON.parse(ev.data);
            if(what == "offer")
            {
                this.peerConnection.setRemoteDescription(
                    new RTCSessionDescription(JSON.parse(data)),
                    () => {
                        this.peerConnection.createAnswer(
                            sessionDescription => 
                            {
                                this.peerConnection.setLocalDescription(sessionDescription);
                                this.webSocket.send(JSON.stringify({
                                    what: "answer",
                                    data: JSON.stringify(sessionDescription)
                                }));
                            }, 
                            err => alert("createAnswer failed: " + err), 
                            this.mediaConstraints
                        );
                    },
                    err => {
                        alert('setRemoteDescription failed!: ' + err);
                        this.stop();
                    }
                );
            }
        };
    }

    closePeerConnection() 
    {
        if (this.peerConnection) {
            this.peerConnection.close();
            this.peerConnection = undefined;
        }
    }

    closeWebsocket()
    {
        if (this.webSocket) {
            this.webSocket.close();
            this.webSocket = undefined;
        }
    }

    stop() 
    {
        this.removeVideo();
        this.closePeerConnection();
        this.closeWebsocket();
    }
}
