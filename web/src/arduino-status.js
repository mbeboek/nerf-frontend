/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const BIT_IS_SPEED    = 1;
const BIT_SPEED_LEFT  = 1 << 1;

const BIT_MOTOR_L     = 1 << 1;
const BIT_MOTOR_L_DIR = 1 << 2;
const BIT_MOTOR_R     = 1 << 3;
const BIT_MOTOR_R_DIR = 1 << 4;
const BIT_TRIGGER     = 1 << 5;

const Motors = {
    LEFT: "left",
    RIGHT: "right",
};

const MotorDirection = {
    STOPPED: 0, 
    FORWARDS: 1,  
    BACKWARDS: 2,
};

var arduinoStatus = new class
{
    constructor()
    {
        this.clear();
    }

    clear()
    {
        this.byteSpeedLeft  = BIT_IS_SPEED;
        this.byteSpeedRight = BIT_IS_SPEED | BIT_SPEED_LEFT;
        this.byteControls   = 0;

        this.oldByteSpeedLeft  = 0;
        this.oldByteSpeedRight = 0;
        this.oldByteControls   = 0;
    }

    /**
     * sets the DC-motor direction
     * @param {string} motor constant from 'Motors'
     * @param {number} direction constant from 'MotorDirection'
     */
    setMotorDirection(motor, direction)
    {
        const bitEnable = motor == Motors.LEFT ? BIT_MOTOR_L : BIT_MOTOR_R;
        const bitDir    = motor == Motors.LEFT ? BIT_MOTOR_L_DIR : BIT_MOTOR_R_DIR;

        // reset both bits
        this.byteControls &= ~(bitEnable | bitDir);

        if(direction != MotorDirection.STOPPED) {
            this.byteControls |= bitEnable;

            if(direction == MotorDirection.FORWARDS) {
                this.byteControls |= bitDir;
            }
        }
    }

    /**
     * sets the motor speed
     * @param {sting} motor constant from 'Motors' 
     * @param {number} speed normalized speed from 0.0 to 1.0
     */
    setMotorSpeed(motor, speed)
    {
        speed = Math.min(1.0, Math.max(speed, 0.0));
        const speedValue = Math.floor(speed * 63) << 2; // two control bits come first
        
        if(motor == Motors.LEFT) {
            this.byteSpeedLeft &= 3; // reset non control bits
            this.byteSpeedLeft |= speedValue;
        } else {
            this.byteSpeedRight &= 3; // reset non control bits
            this.byteSpeedRight |= speedValue;
        }
    }

    /**
     * triggers the nerf gun
     */
    triggerShoot()
    {
        this.byteControls |= BIT_TRIGGER;
    }

    /**
     * send the signal that the nerf is ready for a new shot
     */
    triggerDefaultPos()
    {
        this.byteControls &= ~BIT_TRIGGER;
    }

    async update()
    {
        const bytes = [];

        if(this.oldByteSpeedLeft != this.byteSpeedLeft) 
        {
            bytes.push(this.byteSpeedLeft);
            this.oldByteSpeedLeft = this.byteSpeedLeft;
        }

        if(this.oldByteSpeedRight != this.byteSpeedRight) 
        {
            bytes.push(this.byteSpeedRight);
            this.oldByteSpeedRight = this.byteSpeedRight;
        }

        if(this.oldByteControls != this.byteControls) 
        {
            bytes.push(this.byteControls);
            this.oldByteControls = this.byteControls;
        }

        API.send(bytes);

        updateBitmask(controlByteContainer, this.byteControls);
        updateBitmask(speedLByteContainer, this.byteSpeedLeft);
        updateBitmask(speedRByteContainer, this.byteSpeedRight);
    }
}();