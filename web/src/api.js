/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const API_PORT = 3904;

const API = new class
{
    constructor()
    {
        this.url = `ws://${location.hostname}:${API_PORT}`;
        this.socket = new WebSocket(this.url);
    }

    async send(bytes) 
    {
        if(bytes.length == 0)
            return;
        
        this.socket.send(new Uint8Array(bytes));
    }
}();
