/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const handler = require('serve-handler');
const http = require('http');
 
const SERVER_PORT = 3905;

const server = http.createServer((request, response) => 
{
    return handler(request, response, {
        public: "web"
    });
})
 
server.listen(SERVER_PORT, () => 
{
  console.log(`Running at http://localhost:${SERVER_PORT}`);
});
